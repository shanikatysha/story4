from django.urls import path

from . import views


#path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi'),.

app_name = 'story8' # main nama folder app nya

urlpatterns = [
    path('search/', views.search, name="search"),
    path('data', views.f_data, name="data"),

]
