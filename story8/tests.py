from django.test import TestCase, Client
from .apps import Story8Config

# Create your tests here.
class story8test(TestCase):
    def test_app_name(self):
        self.assertEqual(Story8Config.name,  "story8")
    def test_url(self):
        response = Client().get('/search/')
        self.assertEquals(200,response.status_code)
    
    def test_url2(self):
        response = Client().get('/data?q=harry/')
        self.assertEquals(200,response.status_code)
    
    def test_template(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'search.html')

    def test_isi_view_html(self):
        response = Client().get('/search/')
        html_response = response.content.decode('utf8')
        self.assertIn('<button type="submit" class="btn btn-primary btn-submit">Search</button>',html_response)
