from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


# Create your views here.

def search(request):
    response = {}
    return render(request, 'search.html', response)

def f_data(request):
    arg = request.GET['q']
    urlItem = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    respon = requests.get(urlItem)
    data = json.loads(respon.content)
    return JsonResponse(data)

