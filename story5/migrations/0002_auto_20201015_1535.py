# Generated by Django 3.1.2 on 2020-10-15 15:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hasil',
            name='hari',
            field=models.CharField(default='Unknown', max_length=15),
        ),
        migrations.AlterField(
            model_name='hasil',
            name='lokasi',
            field=models.CharField(default='Unknown', max_length=50),
        ),
    ]
