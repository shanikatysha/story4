from django.shortcuts import render, redirect
from .models import Hasil
from django.core.exceptions import ValidationError

# Create your views here.


def deleteItems(request, items_id):
    a = Hasil.objects.get(id=items_id)
    a.delete()
    return redirect("story5:jadwal")

def jadwal(request):
    context = {}
    if request.method == "POST":
        hari = request.POST.get('hari')
        tanggal = request.POST.get('tanggal')
        waktu = request.POST.get('waktu')
        kegiatan = request.POST.get('kegiatan')
        lokasi = request.POST.get('lokasi')
        kategori = request.POST.get('kategori')
        hasil = Hasil(hari=hari, tanggal=tanggal, waktu=waktu, kegiatan=kegiatan, lokasi=lokasi, kategori=kategori)
        try:
            #hasil.clean()
            hasil.save()
        except ValidationError:
            pass
    context['hasilan'] = Hasil.objects.all().order_by('tanggal')    
    return render(request, "jadwal.html", context)