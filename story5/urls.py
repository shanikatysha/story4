from django.urls import path

from . import views

#path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi'),.

app_name = 'story5' # main nama folder app nya

urlpatterns = [
    #path('', views.index, name='index'),
    path("jadwal/", views.jadwal, name="jadwal"),
    path ("delete/<int:items_id>/", views.deleteItems, name="delete"),
]
