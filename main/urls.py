from django.urls import path

from . import views

#path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi'),.

app_name = 'main' # main nama folder app nya

urlpatterns = [
    path('', views.index, name='index'),
    path('portfolio/', views.portfolio, name='portfolio'),
]
