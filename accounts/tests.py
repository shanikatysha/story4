from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .apps import AccountsConfig
# Create your tests here.

class TestSignUp(TestCase):
    def test_app_name(self):
        self.assertEqual(AccountsConfig.name, 'accounts')

    def test_url_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertEquals(200,response.status_code)

    def test_nama_templates_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertTemplateUsed(response, 'registration/signup.html') #nama html
    
    def test_isi_view_html_signup(self):
        response = Client().get('/accounts/signup/')
        html_response = response.content.decode('utf8')
        self.assertIn('<form method="post">',html_response)
        self.assertIn('<button class="button" type="submit">Create Account</button>', html_response)
    
    def setUp(self) -> None:
        self.username = 'testuser'
        self.password = 'password'
    
    def test_signup_page_view_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')

class UserCreationFormTest(TestCase):

    def test_form(self):
        data = {
            'username': 'testuser',
            'password1': 'hello123',
            'password2': 'hello123',
        }

        form = UserCreationForm(data)

        self.assertFalse(form.is_valid())